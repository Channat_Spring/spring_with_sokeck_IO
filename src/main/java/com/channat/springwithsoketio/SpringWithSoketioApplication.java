package com.channat.springwithsoketio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringWithSoketioApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringWithSoketioApplication.class, args);
    }
}
